import tailwindAspectRatio from '@tailwindcss/aspect-ratio'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Faetale',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          "Queer tabletop roleplaying gaming content for GM's, players, and curious nerds!",
      },
      { name: 'format-detection', content: 'telephone=no' },
      { property: 'og:title', content: 'Faetale - Once Upon a Time' },
      {
        property: 'og:description',
        content:
          "Queer tabletop roleplaying gaming content for GM's, players, and curious nerds!",
      },
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: 'https://faetale.com/' },
      { property: 'og:image', content: 'https://faetale.com/preview.jpg' },
      { property: 'og:image:width', content: '1280' },
      { property: 'og:image:height', content: '640' },
      { property: 'twitter:card', content: 'summary_large_image' },
      { property: 'twitter:image', content: 'https://faetale.com/preview.jpg' },
      { property: 'twitter:site', content: '@FaetaleTTRPG' },
      { property: 'theme-color', content: '#ffffff' },
      { property: 'msapplication-TileColor', content: '#ffffff' },
      {
        property: 'msapplication-TileImage',
        content: '/icon/ms-icon-144x144.png',
      },
    ],
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '57x57',
        href: '/icon/apple-icon-57x57.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '60x60',
        href: '/icon/apple-icon-60x60.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '72x72',
        href: '/icon/apple-icon-72x72.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '76x76',
        href: '/icon/apple-icon-76x76.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '114x114',
        href: '/icon/apple-icon-114x114.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '120x120',
        href: '/icon/apple-icon-120x120.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '144x144',
        href: '/icon/apple-icon-144x144.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '152x152',
        href: '/icon/apple-icon-152x152.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/icon/apple-icon-180x180.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '192x192',
        href: '/icon/android-icon-192x192.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/icon/favicon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '96x96',
        href: '/icon/favicon-96x96.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/icon/favicon-16x16.png',
      },
      { rel: 'manifest', href: '/icon/manifest.json' },
    ],
    script: [
      {
        hid: 'twitter',
        defer: true,
        src: 'https://platform.twitter.com/widgets.js',
      },
    ],
  },

  router: {
    routes: [{ path: '/:pageToken', props: true }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  // Tailwind
  tailwindcss: {
    config: {
      plugins: [tailwindAspectRatio],
      theme: {
        extend: {
          colors: {
            brandlight: '#ca4e8d',
            branddark: '#83398c',
          },
        },
      },
    },
  },
}
